/**
  * Created by mark on 07/04/2017.
  */
import java.security.MessageDigest

import scala.util.control.Breaks._
object Validator extends App{
  while (true){
    print("Enter result: ")
    val input=readLine()
    if (input=="exit")
      break()
    val ans=input.split(",")
    val studentMd5=ans.last
    val params=ans.dropRight(1)
    val md5Str=new String(MessageDigest.getInstance("MD5").digest(params.mkString("+").getBytes)).map(0xFF & _).map { "%02x".format(_) }.foldLeft(""){_ + _}

    if (studentMd5!=md5Str)  {
      println("X")
      println(s"excepted: $md5Str")
      println(s"actually: $studentMd5")
    } else println("O")



  }



}
